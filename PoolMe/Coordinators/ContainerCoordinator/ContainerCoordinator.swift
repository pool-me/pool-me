//
//  ContainerCoordinator.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/7/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit

//PrivacyPolicyFlow must added

class ContainerCoordinator: BaseCoordinator, Rooting {
    //MARK: - Properties -
    
    weak var parentCoordinator: MainCoordinator?
    
    
    
    //MARK: - Methods -
    
    func root() -> UIViewController {
        let containerController = ContainerSceneBuilder.createContainerScene(coordinator: self)
        return containerController
    }
    
    func driveContainerScene(from controller: UIViewController) {
        let conrainerController = ContainerSceneBuilder.createContainerScene(coordinator: self)
        controller.present(conrainerController, animated: true, completion: nil)
    }
    
    func dissmissContainerScene(by scene: UIViewController) {
        scene.dismiss(animated: true, completion: nil)
        self.parentCoordinator?.childDidFinish(self)
    }
    
//    func driveProfileScene(from controller: UIViewController) {
//        let profileController = ContainerSceneBuilder.createProfileScene(coordinator: self)
//        controller.present(profileController, animated: true, completion: nil)
//    }
    
    func dismissProfileScene(by scene: UIViewController) {
        scene.dismiss(animated: true, completion: nil)
    }
}



