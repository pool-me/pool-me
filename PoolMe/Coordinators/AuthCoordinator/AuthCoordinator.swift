//
//  AuthCoordinator.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/14/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class AuthCoordinator: BaseCoordinator, Rooting {
   
    //MARK: - Properties -
    
    weak var parentCoordinator: MainCoordinator?
   
    
    //MARK: - Methods -

    func root() -> UIViewController {
        let loginController = AuthSceneBuilder.createLoginScene(with: self)
        return loginController
    }
    
    func driveSignupScene(from controller: UIViewController) {
        let signController = AuthSceneBuilder.createSignupScene(with: self)
        controller.present(signController, animated: true, completion: nil)
    }
    
    func driveLogInScene(from controller: UIViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func driveContainerScene(from controller: UIViewController) {
        self.parentCoordinator?.childContainer(from: controller)
    }
    
    func driveForgotPasswordScene(from controller: UIViewController) {
        let forgotPassword = AuthSceneBuilder.createForgetPasswordScene(with: self)
        controller.present(forgotPassword, animated: true, completion: nil)
    }
}
