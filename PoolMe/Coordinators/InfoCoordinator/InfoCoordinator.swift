//
//  InfoCoordinator.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


class InfoCoordinator: BaseCoordinator {
    //MARK: - Methods -
    
    override func start() {
        let mapController = ABTabBarSceneBuilder.createInfoScene(with: self)
        self.navigationController?.pushViewController(mapController, animated: true)
    }
}
