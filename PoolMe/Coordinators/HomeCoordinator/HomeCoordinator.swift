//
//  HomeCoordinator.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/14/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class HomeCoordinator: BaseCoordinator, UINavigationControllerDelegate {
    
    override var childCoordinators: [Coordinator] {
        didSet {
            print("HomeCoordinator child coordinator count: \(self.childCoordinators.count)")
        }
    }
    
  
    //MARK: - Methods -
    
    func start(from vc: ABTabBarController? = nil) {
        self.navigationController?.delegate = self
        let homeController = ABTabBarSceneBuilder.createHomeScene(with: self)
        vc?.delegate = homeController as! MapController
        self.navigationController?.pushViewController(homeController, animated: true)
    }
    
    func drivePoolMeScene() {
        let poolMeController = HomeBuilder.createPoolMeScene(with: self)
        self.navigationController?.pushViewController(poolMeController, animated: true)
    }
    
//    func childDidFinish(_ child: Coordinator) {
//        for (index, coordinator) in self.childCoordinators.enumerated() {
//            if coordinator === child {
//                self.childCoordinators.remove(at: index)
//                break
//            }
//        }
//    }
    
//    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
//        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else { return }
//        if navigationController.viewControllers.contains(fromViewController) { return }
//        switch fromViewController {
//        case (fromViewController as? PoolMeController):
//            self.childDidFinish((fromViewController as! PoolMeController).coordinator!)
//        default: break
//        }
//    }
}


