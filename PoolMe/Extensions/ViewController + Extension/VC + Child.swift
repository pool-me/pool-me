//
//  VC + Child.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/30/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



extension UIViewController {
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func remove() {
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}


extension UIViewController {
    var statusHeight: CGFloat {
        switch UIDevice().type {
        case .iPhone6, .iPhone5, .iPhone5S, .iPhone6S, .iPhone7, .iPhoneSE, .iPhone8, .iPhone6Plus, .iPhone6SPlus, .iPhone7Plus, .iPhone8Plus: return 20
        default:  return view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 44
        }
    }
    
    var safeAreaBottomInset: CGFloat {
        return self.view.frame.height - self.view.safeAreaLayoutGuide.layoutFrame.height - statusHeight
    }
}

extension UIViewController {
    func hideTabBar() {
        NotificationCenter.default.post(name: .tabBarWillHide, object: nil)
    }
    
    func showTabBar() {
        NotificationCenter.default.post(name: .tabBarWillShow, object: nil)
    }
}

extension UIViewController {
    open override func awakeFromNib() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}


extension UIViewController {
    var window: UIView? {
        return UIApplication.shared.windows.first(where: { $0.isKeyWindow })
    }
}
