//
//  ABFont.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 11/23/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


extension UIFont {
    static let timesNewRomanLarge = UIFont(name: "Times New Roman", size: 35)!
    static let timesNewRomanStandart = UIFont(name: "Times New Roman", size: 25)!
    static let timesNewRomanSegmentedStyle = UIFont(name: "Times New Roman", size: 15)!
    
    static func timesNewRomanWith(size: CGFloat) -> UIFont {
        UIFont(name: "Times New Roman", size: size)!
    }
}
