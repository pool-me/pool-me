//
//  Color + Extension.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 11/11/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


extension UIColor {
    static func rgb(_ red: CGFloat,_ green: CGFloat,_ blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    static var mainColor      = UIColor(named: "mainColor")!
    static var secondColor    = UIColor(named: "secondColor")!
    static var logInButton    = UIColor(named: "logInButton")!
    static var forgetPassword = UIColor(named: "forgetPassword")!
}
