//
//  Storyboard + Extension.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/6/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



extension UIStoryboard {
    @nonobjc class var main: UIStoryboard {
        return UIStoryboard(name: Storyboard.main, bundle: nil)
    }
    @nonobjc class var menu: UIStoryboard {
        return UIStoryboard(name: Storyboard.menu, bundle: nil)
    }
    @nonobjc class var history: UIStoryboard {
        return UIStoryboard(name: Storyboard.history, bundle: nil)
    }
}
