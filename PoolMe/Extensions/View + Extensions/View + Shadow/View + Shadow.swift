//
//  View + Shadow.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 11/20/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit





extension UIView {
    func shadow(_ height: Int = 5) {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height: height)
    }

    func removeShadow() {
        self.layer.shadowOffset = CGSize(width: 0 , height: 0)
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.cornerRadius = 0.0
        self.layer.shadowRadius = 0.0
        self.layer.shadowOpacity = 0.0
    }
}



enum ViewStyle {
    case standart
}

extension UIView {
    func setShadow(color: CGColor = UIColor.lightGray.cgColor, offset: CGSize = .init(width: 0, height: 1), radius: CGFloat = 1, opacity: Float = 0.6) {
        self.layer.shadowColor = color
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.masksToBounds = false
    }
    
    func halfTriangleView(up: Int? = 120, down: Int? = 144, y: CGFloat?) {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: up!, y: 0))
        path.addLine(to: CGPoint(x: CGFloat(down!), y: y!))
        path.addLine(to: CGPoint(x: 0, y: y!))
        path.addLine(to: CGPoint(x: 0, y: 0))
        path.close()

        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    convenience init(view style: ViewStyle) {
        self.init()
        self.standartShadow()
    }
    
    func standartShadow(with color: UIColor = UIColor.gray) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 0.7
        self.layer.masksToBounds = false
    }
}
