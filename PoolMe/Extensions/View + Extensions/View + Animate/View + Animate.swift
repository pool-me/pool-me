//
//  View + Animate.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 3/17/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


extension UIView {
    static func standartAnimate(duration: TimeInterval = 0.6, completion: @escaping ()->()) {
        UIView.animate(withDuration: duration,
                       delay: 0,
                       usingSpringWithDamping: 0.75,
                       initialSpringVelocity: 0.75,
                       options: .curveEaseOut,
                       animations: {
                        completion()
        },
                       completion: nil)
    }
}
