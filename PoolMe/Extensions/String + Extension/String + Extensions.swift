//
//  String + Extensions.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


extension String {
    var isValidEmail: Bool {
        return NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }
    
    func getTextHeight(boldSystemFont: CGFloat) -> CGFloat {
        let height = NSString(string: self).boundingRect(with: CGSize(width: UIScreen.main.bounds.width, height: 1000), options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), attributes: [.font : UIFont.boldSystemFont(ofSize: boldSystemFont)], context: nil).height
        
        return height
    }
}
