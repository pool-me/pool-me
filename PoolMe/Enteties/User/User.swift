//
//  User.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import Foundation



protocol Identifiable {
    var id: String? { get set }
}

struct CurrentUser {
    static var user: Userable?
}

protocol Userable: Identifiable {
    var username: String { get }
    var email: String { get }
    var password: String { get }
    var phoneNumber: String { get }
    var profileImageUrl: String? { get set }
}


struct Passenger: Userable, Codable {
    var username: String
    var email: String
    var password: String
    var phoneNumber: String
    var profileImageUrl: String?
    var id: String?
}

struct Driver: Userable, Codable {
    var username: String
    var email: String
    var password: String
    var phoneNumber: String
    var profileImageUrl: String?
    var id: String?
    //var history: [History] = []
    
//    var dictionary: Dictionary<String, Any> {
//        return [
//            "id": id ?? "",
//            "username": username,
//            "email": email,
//            "password": password,
//            "phoneNumber": phoneNumber,
//            "profileImageUrl": profileImageUrl ?? "",
//            //"history": self.history
//        ]
//    }
}



