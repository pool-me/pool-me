//
//  ContainerSceneBuilder.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/14/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


protocol ContainerSceneBuilding {
    static func createContainerScene(coordinator: ContainerCoordinator) -> UIViewController
    static func createMenuBarScene(delegate: SideMenuControllerDelegate) -> UIViewController
    static func createABTabBarController() -> ABTabBarController
    //static func createProfileScene(coordinator: ContainerCoordinator) -> UIViewController
}

class ContainerSceneBuilder: ContainerSceneBuilding {
    static func createContainerScene(coordinator: ContainerCoordinator) -> UIViewController {
        let containerController = ContainerController(coordinator: coordinator)
        containerController.modalPresentationStyle = .fullScreen
        return containerController
    }
    
    static func createMenuBarScene(delegate: SideMenuControllerDelegate) -> UIViewController {
        let sideMenuController = UIStoryboard.menu.instantiateViewController(identifier: Controller.menu) { (coder) in
            SideMenuController(coder: coder, delegate: delegate)
        }
        
        return sideMenuController
    }
    
    static func createABTabBarController() -> ABTabBarController {
        let customTabBarController = ABTabBarController()
        return customTabBarController
    }
    
//    static func createProfileScene(coordinator: ContainerCoordinator) -> UIViewController {
//        let profileController = UIStoryboard.main.instantiateViewController(identifier: Controller.profile) { (coder) in
//            ProfileController(coder: coder, coordinator: coordinator, networkService: FIRFirestoreService.shared)
//        }
//        return profileController
    //}
}
