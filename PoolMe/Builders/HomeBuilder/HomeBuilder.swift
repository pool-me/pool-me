//
//  HomeBuilder.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



protocol HomeBuilding: class {
    static func createPoolMeScene(with coordinator: HomeCoordinator) -> UIViewController
}

class HomeBuilder: HomeBuilding {
    static func createPoolMeScene(with coordinator: HomeCoordinator) -> UIViewController {
        let poolMeController = PoolMeController(coordinator: coordinator)
        return poolMeController
    }
    
}
