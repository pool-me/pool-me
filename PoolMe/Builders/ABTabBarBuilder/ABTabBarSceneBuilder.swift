//
//  ABTabBarSceneBuilder.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/14/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



protocol ABTabBarSceneBuilding {
    static func createHomeScene(with coordinator: HomeCoordinator) -> UIViewController
    static func createInfoScene(with coordinator: InfoCoordinator) -> UIViewController
    static func createHistoryScene(with coordinator: HistoryCoordinator)  -> UITableViewController
}


class ABTabBarSceneBuilder: ABTabBarSceneBuilding {
    static func createHomeScene(with coordinator: HomeCoordinator) -> UIViewController {
        let homeController = MapController(coordinator: coordinator)
        let image = UIImage(systemName: "house.fill")?.withRenderingMode(.alwaysTemplate)
        image?.withTintColor(.purple)
        homeController.tabBarItem = UITabBarItem(title: "Home", image: image, tag: 1)
        return homeController
    }
    
    static func createInfoScene(with coordinator: InfoCoordinator) -> UIViewController {
        let infoController = InfoController(coordinator: coordinator)
        infoController.tabBarItem = UITabBarItem(title: "Info", image: UIImage(systemName: "info"), tag: 0)
        return infoController
    }
    
    static func createHistoryScene(with coordinator: HistoryCoordinator) -> UITableViewController {
        //let historyController = HistoryController(coordinator: coordinator)
        
        let historyController = UIStoryboard.history.instantiateViewController(identifier: Controller.history) { (coder) in
            return HistoryController(coder: coder, coordinator: coordinator)
        }
        historyController.tabBarItem = UITabBarItem(title: "History", image: #imageLiteral(resourceName: "history"), tag: 1)
        return historyController
    }
}

