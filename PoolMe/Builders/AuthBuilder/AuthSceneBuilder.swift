//
//  AuthSceneBuilder.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/14/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



protocol AuthSceneBuilding {
    static func createLoginScene(with coordinator: AuthCoordinator) -> UIViewController
    static func createSignupScene(with coordinator: AuthCoordinator) -> UIViewController
    static func createForgetPasswordScene(with coordinator: AuthCoordinator) -> UIViewController
}



class AuthSceneBuilder: AuthSceneBuilding {
    static func createLoginScene(with coordinator: AuthCoordinator) -> UIViewController {
        let loginController = LoginController(coordinator: coordinator)
        return loginController
    }
    
    static func createSignupScene(with coordinator: AuthCoordinator) -> UIViewController {
        let signupController = SignupController(coordinator: coordinator)
        signupController.modalPresentationStyle = .fullScreen
        return signupController
    }
    
    static func createForgetPasswordScene(with coordinator: AuthCoordinator) -> UIViewController {
        let forgetPasswordController = ForgetPasswordController(coordinator: coordinator)
        return forgetPasswordController
    }
}


