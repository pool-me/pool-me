//
//  InfoController.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class InfoController: UIViewController {
    //MARK: - Properties -
    
    let infoTextView: UITextView = {
        let infoTextView = UITextView()
        infoTextView.isSelectable = false
        infoTextView.isEditable = false
        infoTextView.font = UIFont.timesNewRomanWith(size: 22)
        infoTextView.translatesAutoresizingMaskIntoConstraints = false
        infoTextView.text = "Сервис PoolMe представляет собой площадку для совместных поездок (карпулинг), на которой водители (частные лица) и пассажиры заранее договариваются о поездке: водитель предлагает свободные места в своём автомобиле, а пассажиры, планирующие отправиться в том же направлении, бронируют их. Пассажир указывает желаемый пункт отправления и назначения, после чего сервис отображает подходящие предложения от водителей. После поездки пользователи оставляют друг о друге публичные отзывы. Администрация сервиса контролирует стоимость поездки (в среднем пассажир платит не более одной трети стоимости топлива на конкретную поездку) и количество пассажиров (не более 8 человек)[3]. Водители, использующие BlaBlaCar, не зарабатывают, оказывая транспортные услуги, а лишь частично или полностью компенсируют расходы на поездку, которые распределяются пропорционально среди всех участников поездки, в отличие от такси, при использовании которого расходы оплачивает только пассажир, а направление поездки не ограничивается водителем, и автостопа, где по факту платит только водитель, который следует исключительно по своему маршруту."
        return infoTextView
    }()
    
    
    
    //MARK: - Dependencies -
    
    weak var coordinator: InfoCoordinator?
    
    
    
    //MARK: - Init -
    
    init(coordinator: InfoCoordinator) {
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK: - Lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.drawInfoTextView()
        self.configureNavigationBar()
    }
    
    
    
    //MARK: - Methods -
    
    private func drawInfoTextView() {
        self.view.addSubview(self.infoTextView)
        self.infoTextView.anchor(top: self.view.topAnchor,
                                 leading: self.view.leadingAnchor,
                                 bottom: self.view.bottomAnchor,
                                 trailing: self.view.trailingAnchor,
                                 padding: .init(top: 16, left: 16, bottom: 16, right: 16))
    }
    
    private func configureNavigationBar() {
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.prefersLargeTitles = true
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithTransparentBackground()
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white,
                                                     .font: UIFont.timesNewRomanLarge]
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white,
                                                .font: UIFont.timesNewRomanStandart]
        navBarAppearance.backgroundColor = .systemPurple
        self.navigationController?.navigationBar.standardAppearance = navBarAppearance
        self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        
        self.navigationItem.title = "Info"
    }
}
