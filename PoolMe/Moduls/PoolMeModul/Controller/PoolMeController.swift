//
//  PoolMeController.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class PoolMeController: UIViewController {
    
    
    
    
    
    //MARK: - Dependencies - 
    weak var coordinator: HomeCoordinator?
    
    
    
    //MARK: - Init -
    init(coordinator: HomeCoordinator) {
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
