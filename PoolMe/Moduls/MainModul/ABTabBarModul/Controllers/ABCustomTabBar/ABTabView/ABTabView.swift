//
//  ABTabView.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 1/5/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


class ABTabView: UIView {
    
    private lazy var titleLabel: ABLabel = {
        let lbl = ABLabel()
        lbl.font = ABTabBarSetting.tabBarTextFont
        lbl.textColor = ABTabBarSetting.tabBarTextColor
        lbl.backgroundColor = ABTabBarSetting.tabBarTextBackgroundColor
        lbl.layer.masksToBounds = true
        lbl.clipsToBounds = true
        return lbl
    }()
    
    private lazy var tabImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private var imageBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = ABTabBarSetting.tabBarTintColor
        view.layer.cornerRadius = (ABTabBarSetting.tabBarSizeImage + 8) / 2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    init(tabBar: UITabBarItem){
        super.init(frame: .zero)
        guard let selecteImage = tabBar.image else {
            fatalError("You should set image to all view controllers")
        }
        tabImageView = UIImageView(image: selecteImage)
        titleLabel.text = tabBar.title ?? ""
        drawConstraints()
        
    }
    
    
    private func drawConstraints() {
        self.addSubview(imageBackgroundView)
        self.addSubview(titleLabel)
       
        var constraints = [NSLayoutConstraint]()
    
        constraints.append(imageBackgroundView.centerYAnchor.constraint(equalTo: self.centerYAnchor))
        constraints.append(imageBackgroundView.centerXAnchor.constraint(equalTo: self.centerXAnchor))
        constraints.append(imageBackgroundView.heightAnchor.constraint(equalToConstant: ABTabBarSetting.tabBarSizeImage + 8))
        constraints.append(imageBackgroundView.widthAnchor.constraint(equalToConstant: ABTabBarSetting.tabBarSizeImage + 8))
        
        self.imageBackgroundView.addSubview(tabImageView)
        self.tabImageView.fillSuperview(padding: .init(top: 5, left: 5, bottom: 5, right: 5))
        self.imageBackgroundView.setShadow(color: #colorLiteral(red: 0.175711602, green: 0.075403817, blue: 0.2991136611, alpha: 1).cgColor , offset: .init(width: 0, height: 2), radius: 3, opacity: 0.7)
        
        constraints.append(titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: ABTabBarSetting.tabBarHeight))
        constraints.append(titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor))
        constraints.append(titleLabel.heightAnchor.constraint(equalToConstant: 20))
        constraints.forEach({ $0.isActive = true })
        //self.titleLabel.setShadow(color: #colorLiteral(red: 0.175711602, green: 0.075403817, blue: 0.2991136611, alpha: 1).cgColor , offset: .init(width: 0, height: 2), radius: 3, opacity: 0.7)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   internal func animateTabSelected() {
        self.imageBackgroundView.alpha = 1
        titleLabel.alpha = 0
        UIView.animate(withDuration: ABTabBarSetting.tabBarAnimationDurationTime) { [weak self] in
            self?.titleLabel.alpha = 1
            self?.titleLabel.frame.origin.y = ABTabBarSetting.tabBarHeight / 1.7
            self?.imageBackgroundView.frame.origin.y = -5
            self?.imageBackgroundView.alpha = 0
        }
    }
    
    internal func animateTabDeSelect() {
        self.imageBackgroundView.alpha = 1
        UIView.animate(withDuration: ABTabBarSetting.tabBarAnimationDurationTime) { [weak self] in
            self?.titleLabel.frame.origin.y = ABTabBarSetting.tabBarHeight
            self?.titleLabel.alpha = 0
            self?.imageBackgroundView.frame.origin.y = (ABTabBarSetting.tabBarHeight / 2) - CGFloat(ABTabBarSetting.tabBarSizeImage / 2)
            self?.imageBackgroundView.alpha = 1
        }
    }
}
