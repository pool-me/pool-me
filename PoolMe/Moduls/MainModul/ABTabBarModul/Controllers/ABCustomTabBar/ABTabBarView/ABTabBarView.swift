//
//  ABTabBarView.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 1/5/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


class ABTabBarView: UIView {
    
   internal var viewControllers: [UIViewController]! {
        didSet {
            drawTabs()
        }
    }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [])
        stackView.axis = .horizontal
        stackView.spacing = 0
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let innerCircleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.purple.withAlphaComponent(0.4) // #colorLiteral(red: 0.297534734, green: 0.5247463584, blue: 0.1552932858, alpha: 1)
        return view
    }()
    
    private let outerCircleView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemPurple //ABTabBarSetting.tabBarTintColor
        view.tintColor = ABTabBarSetting.tabBarSelectedImageTintColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let tabSelectedImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private var tabWidth: CGFloat {
        return UIScreen.main.bounds.width / CGFloat(viewControllers.count)
    }
    
    weak var tabBarDelegate: ABTabBarDelegate?
    
    var selectedIndex: Int = 0
    private var previousSelectedIndex = 0
    private var currentIndex: Int = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureTabView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureTabView()
    }
    
    private func configureTabView() {
        self.tintColor = ABTabBarSetting.tabBarImageTintColor
        self.backgroundColor = ABTabBarSetting.tabBarBackground
        self.dropShadow()
    }
    
    private func dropShadow() {
        self.setShadow(color: ABTabBarSetting.tabBarShadowColor, offset: .init(width: 0, height: -3), radius: 3, opacity: 0.2)
//        self.setShadow(color: #colorLiteral(red: 0.175711602, green: 0.075403817, blue: 0.2991136611, alpha: 1).cgColor , offset: .init(width: 0, height: -3), radius: 3, opacity: 0.7)
    }
    
    
    #warning("Dipachi het xndir ka ervi")
    internal func setupView() {
        drawConstraint()
        DispatchQueue.main.async { [weak self] in
            self?.didSelectTab(index: self!.selectedIndex)
        }
    }
    
    private func drawTabs() {
        for vc in viewControllers {
            let barView = ABTabView(tabBar: vc.tabBarItem)
            barView.heightAnchor.constraint(equalToConstant: ABTabBarSetting.tabBarHeight).isActive = true
            barView.translatesAutoresizingMaskIntoConstraints = false
            barView.isUserInteractionEnabled = false
            self.stackView.addArrangedSubview(barView)
        }
    }
    
    private func drawConstraint() {
        addSubview(stackView)
        addSubview(innerCircleView)
      
        innerCircleView.addSubview(outerCircleView)
        outerCircleView.addSubview(tabSelectedImageView)
 
        var constraints = [NSLayoutConstraint]()
        
        innerCircleView.frame.size = ABTabBarSetting.tabBarCircleSize
       
        innerCircleView.layer.cornerRadius = ABTabBarSetting.tabBarCircleSize.width / 2
        
        outerCircleView.layer.cornerRadius = (innerCircleView.frame.size.height - 5) / 2
        constraints.append(outerCircleView.centerYAnchor.constraint(equalTo: self.innerCircleView.centerYAnchor))
        constraints.append(outerCircleView.centerXAnchor.constraint(equalTo: self.innerCircleView.centerXAnchor))
        constraints.append(outerCircleView.heightAnchor.constraint(equalToConstant: innerCircleView.frame.size.height - 5))
        constraints.append(outerCircleView.widthAnchor.constraint(equalToConstant: innerCircleView.frame.size.width - 5))
        
        constraints.append(tabSelectedImageView.centerYAnchor.constraint(equalTo: outerCircleView.centerYAnchor))
        constraints.append(tabSelectedImageView.centerXAnchor.constraint(equalTo: outerCircleView.centerXAnchor))
        constraints.append(tabSelectedImageView.heightAnchor.constraint(equalToConstant: ABTabBarSetting.tabBarSizeSelectedImage))
        constraints.append(tabSelectedImageView.widthAnchor.constraint(equalToConstant: ABTabBarSetting.tabBarSizeSelectedImage))
        
        stackView.frame = self.bounds.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
       
        constraints.append(stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor))
        constraints.append(stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor))
        constraints.append(stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor))
        constraints.append(stackView.topAnchor.constraint(equalTo: self.topAnchor))
    
        constraints.forEach({ $0.isActive = true })
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        guard let touchArea = touches.first?.location(in: self).x else {
            return
        }
        let index = Int(floor(touchArea / tabWidth))
        didSelectTab(index: index)
    }
    
    
    private func didSelectTab(index: Int) {
        if index + 1 == selectedIndex {return}
        animateTitle(index: index)
        
        if index == 1 {
            self.removeShadow()
        } else {
            self.dropShadow()
        }
 
        previousSelectedIndex = selectedIndex
        selectedIndex  = index + 1
        
        tabBarDelegate?.abTabBar(self, didSelectTabAt: index, currentIndex: currentIndex)
        self.currentIndex = index
        
        animateCircle(with: circlePath())
        animateImage()
        
        guard let image = self.viewControllers[index].tabBarItem.selectedImage else {
            fatalError("You should insert selected image to all View Controllers")
        }
        self.tabSelectedImageView.image = image
    }
    
    private func animateTitle(index: Int) {
        for i in self.stackView.arrangedSubviews {
            if i == stackView.arrangedSubviews[index] {
                if let barView = self.stackView.arrangedSubviews[index] as? ABTabView {
                    barView.animateTabSelected()
                }
            }else {
                if let barView = i as? ABTabView {
                    barView.animateTabDeSelect()
                }
            }
        }
    }
    
    private func animateImage() {
        tabSelectedImageView.alpha = 0
        UIView.animate(withDuration: ABTabBarSetting.tabBarAnimationDurationTime) { [weak self] in
            self?.tabSelectedImageView.alpha = 1
        }
    }
    
    private func circlePath() -> CGPath {
        let startPoint_X =  CGFloat(previousSelectedIndex) * CGFloat(tabWidth) - (tabWidth * 0.5)
        let endPoint_X = CGFloat(selectedIndex ) * CGFloat(tabWidth) - (tabWidth * 0.5)
        let y = ABTabBarSetting.tabBarHeight * 0.1
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: startPoint_X, y: y))
        path.addLine(to: CGPoint(x: endPoint_X, y: y))
        return path.cgPath
    }
    
    private func animateCircle(with path: CGPath) {
        let caframeAnimation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.position))
        caframeAnimation.path = path
        caframeAnimation.duration = ABTabBarSetting.tabBarAnimationDurationTime
        caframeAnimation.fillMode = .both
        caframeAnimation.isRemovedOnCompletion = false
        innerCircleView.layer.add(caframeAnimation, forKey: "circleLayerAnimationKey")
    }
}



