//
//  ABTabBarViewController.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 1/5/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class ABTabBarViewController: UIViewController, ABTabBarDelegate {
    
    //MARK: - Properties -
    
    public var selectedIndex: Int = 0 {
        didSet {
            self.abTabBar.selectedIndex = selectedIndex
        }
    }
    private var bottomAnchorContainer: NSLayoutConstraint!
    public var viewControllers: [UIViewController]! { didSet { self.drawConstraint() } }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        return view
    }()
    
    lazy var abTabBar: ABTabBarView = {
        let abTabBar = ABTabBarView()
        abTabBar.viewControllers = viewControllers
        abTabBar.setupView()
        abTabBar.tabBarDelegate = self
        abTabBar.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(abTabBar)
        return abTabBar
    }()
    
    private lazy var safeAreaView: UIView = {
        let safeAreaView = UIView()
        safeAreaView.backgroundColor = ABTabBarSetting.tabBarBackground
        safeAreaView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(safeAreaView)
        return safeAreaView
    }()
    
    
    
    //MARK: - Lifecycle -
    
    override open func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    //MARK: - Methods -
    
    private func drawConstraint() {
        self.drawContainerViewConstrait()
        self.drawAbTabBarViewConstrait()
        self.drawSafeAreaViewConstrait()
    }
    
    private  func drawContainerViewConstrait() {
        self.containerView.anchor(top: view.topAnchor,
                                  leading: view.leadingAnchor,
                                  bottom: nil,
                                  trailing: view.trailingAnchor,
                                  padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        self.bottomAnchorContainer = self.containerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -(ABTabBarSetting.tabBarHeight))
        self.bottomAnchorContainer.isActive = true
    }

    private func drawAbTabBarViewConstrait() {
        self.abTabBar.anchor(top: nil,
                             leading: self.view.leadingAnchor,
                             bottom: self.view.safeAreaLayoutGuide.bottomAnchor,
                             trailing: self.view.trailingAnchor,
                             size: .init(width: 0, height: ABTabBarSetting.tabBarHeight))
        self.view.bringSubviewToFront(abTabBar)
    }
    
    private func drawSafeAreaViewConstrait() {
        self.safeAreaView.anchor(top     : abTabBar.bottomAnchor,
                                 leading : self.view.leadingAnchor,
                                 bottom  : self.view.bottomAnchor,
                                 trailing: self.view.trailingAnchor)
        self.view.bringSubviewToFront(safeAreaView)
    }
    
    func hideTabBar(_ tabBar: ABTabBarView, state isHidden: Bool) {
        UIView.animate(withDuration: 0.2) {
            isHidden ? self.view.sendSubviewToBack(self.abTabBar)     : self.view.bringSubviewToFront(self.abTabBar)
            isHidden ? self.view.sendSubviewToBack(self.safeAreaView) : self.view.bringSubviewToFront(self.safeAreaView)
            self.bottomAnchorContainer.constant = isHidden ? self.safeAreaBottomInset : -(ABTabBarSetting.tabBarHeight)
            self.view.layoutIfNeeded()
        }
    }
    
    open func abTabBar(_ tabBar: ABTabBarView, didSelectTabAt index: Int, currentIndex: Int) {
        let previousVC = viewControllers?[currentIndex]
        previousVC?.remove()
        let vc = viewControllers[index]
        addChild(vc)
        vc.view.frame = containerView.bounds
        containerView.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
}


// Here you can customize the tab bar to meet your neededs

public struct ABTabBarSetting {
    public static var tabBarHeight: CGFloat = 66
    public static var tabBarTintColor: UIColor = UIColor(red: 250/255, green: 51/255, blue: 24/255, alpha: 1)
    public static var tabBarBackground: UIColor = UIColor.white
    public static var tabBarCircleSize = CGSize(width: 65, height: 65)
    public static var tabBarSizeImage: CGFloat = 25
    public static var tabBarShadowColor = UIColor.lightGray.cgColor
    public static var tabBarSizeSelectedImage: CGFloat = 20
    public static var tabBarAnimationDurationTime: Double = 0.4
    public static var tabBarSelectedImageTintColor: UIColor = .white
    public static var tabBarImageTintColor: UIColor = UIColor.systemPurple.withAlphaComponent(0.8)
    public static var tabBarTextColor: UIColor = .white
    public static var tabBarTextFont: UIFont = UIFont.timesNewRomanSegmentedStyle
    public static var tabBarTextBackgroundColor: UIColor = UIColor.systemPurple.withAlphaComponent(0.85)
}


// use this protocol to detect when a tab bar item is pressed
@available(iOS 10.0, *)
protocol ABTabBarDelegate: AnyObject {
    func abTabBar(_ tabBar: ABTabBarView, didSelectTabAt index: Int, currentIndex: Int)
    func hideTabBar(_ tabBar: ABTabBarView, state isHidden: Bool)
}

