//
//  ABTabBarController.swift
//  FurnitureApp
//
//  Created by Aleksandr Bagdasaryan on 6/29/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



protocol ABTabBarControllerDelegate: class {
    func orderServiceButtonDidTapped()
}


class ABTabBarController: ABTabBarViewController {
    
    //MARK: - Properties -
    
    let bottomView = RoundShadowView()
    let orderButton = OrderButton()
    
    var bottomViewIsShown: Bool = true
    var bottomViewBottomConstraint: NSLayoutConstraint!
    
    var tabBarHeight: CGFloat { return 60 }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    
    let homeCoordinator = HomeCoordinator(navigationController: UINavigationController())
    let historyCoordinator = HistoryCoordinator(navigationController: UINavigationController())
    let infoCoordinator = InfoCoordinator(navigationController: UINavigationController())
    
    weak var delegate: ABTabBarControllerDelegate?
    
    lazy var blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        view.alpha = 0
        view.frame = self.window?.frame ?? self.view.frame
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
        return view
    }()
    
    
    
    //MARK: - Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTabBarNotification()
        self.configureTabBar()
        self.configureControllers()
        self.createBottomView()
        self.configurateNotificationCenter()
        self.orderButtonWillShowNotification()
        self.configureOrderButton()
        self.view.bringSubviewToFront(self.abTabBar)
        self.drawBlackView()
    }

    
    
    
    //MARK: - Methods -
    
    private func drawBlackView() {
        self.view.addSubview(self.blackView)
    }
    
    @objc private func handleDismiss() {
        self.postNotification(name: .sideBarButtonTapped)
    }
    
    private func configureTabBar() {
        ABTabBarSetting.tabBarAnimationDurationTime = 0.3
        ABTabBarSetting.tabBarCircleSize = .init(width: 40, height: 40)
        ABTabBarSetting.tabBarHeight = self.tabBarHeight
        ABTabBarSetting.tabBarTintColor = .systemPurple
        ABTabBarSetting.tabBarSizeSelectedImage = 22
        ABTabBarSetting.tabBarSizeImage = 30
        ABTabBarSetting.tabBarBackground = .systemBackground
            // #colorLiteral(red: 0.509141624, green: 0.2353513837, blue: 0.648830235, alpha: 1)
        ABTabBarSetting.tabBarTextBackgroundColor = .systemPurple
        ABTabBarSetting.tabBarImageTintColor = .white //.systemGreen
        ABTabBarSetting.tabBarSelectedImageTintColor = .white
    }
    
    private func configureControllers() {
        self.homeCoordinator.start(from: self)
        self.historyCoordinator.start()
        self.infoCoordinator.start()
        self.viewControllers = [infoCoordinator.navigationController!,
                                homeCoordinator.navigationController!,
                                historyCoordinator.navigationController!]
        self.selectedIndex = 1
    }
    
    private func configurateNotificationCenter() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(orderButtonWillShowNotification),
                                               name: .orderButtonWillShow,
                                               object: nil)
    }
    
    #warning("Two time asked")
    @objc private func orderButtonWillShowNotification() {
        self.bottomViewIsShown.toggle()
        self.setBottomViewBottomConstraintAnimation(isShown: self.bottomViewIsShown)
    }
    
    private func configureTabBarNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(tabBarWillHideNotification),
                                               name: .tabBarWillHide,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(tabBarWillShowNotification),
                                               name: .tabBarWillShow,
                                               object: nil)
    }
    
    @objc private func tabBarWillHideNotification() {
        self.hideTabBar(self.abTabBar, state: true)
    }
    
    @objc private func tabBarWillShowNotification() {
        self.hideTabBar(self.abTabBar, state: false)
    }
    
   
    
    private func setBottomViewBottomConstraintAnimation(isShown: Bool) {
        let delay = isShown ? 0.15 : 0
        UIView.animate(withDuration: 0.75, delay: delay,
                       usingSpringWithDamping: 0.75,
                       initialSpringVelocity: 1,
                       options: .curveEaseOut,
                       animations: {
                        isShown ? (self.bottomViewBottomConstraint!.constant = -self.tabBarHeight - self.safeAreaBottomInset) :
                            (self.bottomViewBottomConstraint.constant = 100)
                        isShown ? (self.bottomView.alpha = 1) : (self.bottomView.alpha = 0)
                        self.view.layoutIfNeeded()
        }, completion: { (succeded) in })
    }
    
    
    
    var orderButtonHeight = 75
    
    private func createBottomView() {
        switch UIDevice().type {
        case .iPhone5S: self.orderButtonHeight = 60
        case .iPhone6, .iPhone6S, .iPhone7, .iPhoneSE, .iPhone8: self.orderButtonHeight = 75
        case .iPhone6Plus, .iPhone6SPlus, .iPhone7Plus, .iPhone8Plus: self.orderButtonHeight = 85
        case .iPhone11Pro, .iPhoneXS, .iPhoneX: self.orderButtonHeight = 80
        case .iPhone11, .iPhoneXR: self.orderButtonHeight = 83
        case .iPhone11ProMax, .iPhoneXSMax: self.orderButtonHeight = 85
        default: self.orderButtonHeight = 85
        }
        
        self.view.addSubview(self.bottomView)
        self.bottomView.anchor(leading: self.view.leadingAnchor,
                               trailing: self.view.trailingAnchor,
                               size: .init(width: 0, height: self.orderButtonHeight))
        self.bottomViewBottomConstraint = self.bottomView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -48)
        self.bottomViewBottomConstraint.isActive = true
    }
    
    private func configureOrderButton() {
        self.bottomView.addSubview(self.orderButton)
        self.orderButton.fillSuperview(padding: .init(top: 16, left: 16, bottom: 21, right: 16))
        self.orderButton.layer.cornerRadius = CGFloat((orderButtonHeight - 44) / 2)
        self.orderButton.fontSize = CGFloat((orderButtonHeight - 44) / 2)
        self.orderButton.addTarget(self, action: #selector(orderServiceButtonDidTapped), for: .touchUpInside)
        //self.orderButton.setGradientColor(.purple, .systemPurple, frame: CGRect(x: 0, y: 0, width: 1000, height: 38))
    }
    
    @objc private func orderServiceButtonDidTapped() {
        self.delegate?.orderServiceButtonDidTapped()
    }
}



