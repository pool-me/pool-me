//
//  ContainerController.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/1/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit
import MessageUI



class ContainerController: UIViewController {
    //MARK: - Properties -
    
    var customTabBarControler: ABTabBarController!
    var menuBarController: SideMenuController!
    var menuIsShown = false
    var x: CGFloat { return self.view.frame.width - 50 }

    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    
    //MARK: - Dependencies -
    
    let coordinator: ContainerCoordinator
    //let authService: AuthService
    
    
    
    //MARK: - Init -
    
    init(coordinator: ContainerCoordinator) {
        self.coordinator = coordinator
        //self.authService = authService
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK: - Life Cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureSideBarNotification()
        self.configureCustomTabBarController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Container Controller: \(#function)")
    }
    
    
    
    //MARK: - Methods -
    
    func configureCustomTabBarController() {
        let tabBarController = ContainerSceneBuilder.createABTabBarController()
        self.customTabBarControler = tabBarController
        self.addChild(self.customTabBarControler)
        self.view.addSubview(self.customTabBarControler.view)
        self.customTabBarControler.didMove(toParent: self)
    }
    
    func configureMenuBarController() {
        if self.menuBarController == nil {
            self.menuBarController = (ContainerSceneBuilder.createMenuBarScene(delegate: self) as! SideMenuController)
            self.addChild(self.menuBarController)
            self.view.insertSubview(self.menuBarController.view, at: 0)
            self.menuBarController.didMove(toParent: self)
        }
    }
    
    private func configureSideBarNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(sideBarButtonTapped),
                                               name: .sideBarButtonTapped,
                                               object: nil)
    }
    
   
   
    
    @objc private func sideBarButtonTapped() {
        self.configureMenuBarController()
        self.menuIsShown.toggle()
        
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.75,
                       initialSpringVelocity: 0.75,
                       options: .curveEaseOut,
                       animations: {
                        self.customTabBarControler.view.layer.cornerRadius  = self.menuIsShown ? 25 : 0
                        self.customTabBarControler.view.layer.masksToBounds = self.menuIsShown ? true : false
                        self.customTabBarControler.blackView.alpha =  self.menuIsShown ? 1 : 0
                        self.customTabBarControler.abTabBar.isUserInteractionEnabled = self.menuIsShown ? false : true
                        self.customTabBarControler.abTabBar.alpha = self.menuIsShown ? 0.35 : 1
                        self.customTabBarControler.view.transform = self.menuIsShown ? CGAffineTransform(scaleX: 0.85, y: 0.85) : .identity
        },
                       completion: { (success) in
                        self.menuBarAnimation {
                            self.customTabBarControler.view.frame.origin.x = self.menuIsShown ? self.x : 0
                        }
                        
        })
    }
    
    private func menuBarAnimation(completion: @escaping () -> ()) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.75, options: .curveEaseOut, animations: {
            completion()
        }, completion: nil)
    }
    
    func showMailComposer() {
        guard MFMailComposeViewController.canSendMail() else {
            //AlertManager.shared.showCanEmailSendErrorAlert(on: self)
            return
        }
        
        let composer = MFMailComposeViewController()
        composer.mailComposeDelegate = self
        composer.setToRecipients(["bagdasaryanaleksandr@mail.ru"])
        composer.setSubject("Help!")
        composer.setMessageBody("I have some question...", isHTML: false)
        
        present(composer, animated: true)
    }
}


extension ContainerController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let _ = error {
            //AlertManager.shared.showSendEmailFailedAlert(on: self)
            controller.dismiss(animated: true)
            return
        }
        switch result {
        case .cancelled:
            print("cancelled")
        case .saved:
            print("saved")
        case .sent:
            print("sent")
        case .failed:
            print("failed")
        @unknown default:
            print("unknown default")
        }
        
        controller.dismiss(animated: true)
    }
}


extension ContainerController: SideMenuControllerDelegate {
    func privacyAndPolicyDidTapped() {
        self.sideBarButtonTapped()
        Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { (timer) in
            //self.coordinator.drivePrivacyPolicyScene(from: self)
        }
    }
    
    func settingsDidTapped() {
        print(#function)
    }
    
    func logOutDidTapped() {
        self.sideBarButtonTapped()
        Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { (timer) in
            //self.authService.signOut()
            self.coordinator.dissmissContainerScene(by: self)
        }
    }
    
    func homePageDidTapped() {
        self.sideBarButtonTapped()
    }
    
    func profilePageDidTapped() {
        self.sideBarButtonTapped()
        Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { (timer) in
            //self.coordinator.driveProfileScene(from: self)
        }
    }
    
    func contactUsPageDidTapped() {
        self.sideBarButtonTapped()
        Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { (timer) in
             self.showMailComposer()
        }
    }
    
    func callUsCellDidTapped() {
        self.sideBarButtonTapped()
        Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { (timer) in
            guard let url = URL(string:"TEL://+37498886564") else { return }
            UIApplication.shared.open(url)
        }
    }
    
}

