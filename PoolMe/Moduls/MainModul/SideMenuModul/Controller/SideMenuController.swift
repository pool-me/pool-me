//
//  SideMenuController.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit





protocol SideMenuControllerDelegate: NSObjectProtocol {
    func logOutDidTapped()
    func homePageDidTapped()
    func profilePageDidTapped()
    func contactUsPageDidTapped()
    func settingsDidTapped()
    func privacyAndPolicyDidTapped()
    func callUsCellDidTapped()
}


class SideMenuController: UITableViewController {
    //MARK: - Outlets -
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    
    
    //MARK: - Properties -
    
    weak var delegate: SideMenuControllerDelegate!
    
    
    
    //MARK: - Init -
    
    init?(coder: NSCoder, delegate: SideMenuControllerDelegate) {
        self.delegate = delegate
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        print("Menu Bar Controller required init")
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK: - Life Cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.configureImageView()
        self.updateUI()
        print("Menu bar view did load")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    
    //MARK: - Methods -
    
    private func configureTableView() {
        self.tableView.tableFooterView = UIView()
        self.tableView.contentInset = .init(top: 80, left: 0,
                                            bottom: 0, right: 0)
        self.view.backgroundColor = .logInButton
    }
    
    private func configureImageView() {
        self.profileImageView.layer.cornerRadius  = self.profileImageView.frame.width / 2
        self.profileImageView.layer.masksToBounds = true
        self.profileImageView.layer.borderColor   = #colorLiteral(red: 0.2293697, green: 0.09676597267, blue: 0.4011875391, alpha: 1).cgColor
        self.profileImageView.layer.borderWidth   = 2
    }
    
    private func updateUI() {
        self.nameLabel.text  = CurrentUser.user?.username ?? "Alexander"
        self.emailLabel.text = CurrentUser.user?.email ?? "bagdasaryanaleksandr97g@mail.ru"
        //self.profileImageView.retrieveImage(with: CurrentUser.user?.profileImageUrl)
    }
}


//MARK: - Delegate -

extension SideMenuController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.section, indexPath.row) {
        case (1, 0): self.delegate.profilePageDidTapped()
        case (1, 1): self.delegate.homePageDidTapped()
        case (1, 2): self.delegate.contactUsPageDidTapped()
        case (1, 3): self.delegate.callUsCellDidTapped()
        case (2, 0): self.delegate.settingsDidTapped()
        case (2, 1): self.delegate.privacyAndPolicyDidTapped()
        case (2, 2): self.delegate.logOutDidTapped()
        case (_, _): break
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 13
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.backgroundColor = .logInButton
    }
}
