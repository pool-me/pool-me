//
//  History + DataSource.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/23/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



//MARK: - DataSource -

extension HistoryController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.history?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID.historyCell, for: indexPath) as! HistoryCell
        cell.configureCell(model: self.history?[indexPath.row])
        return cell
    }
}

