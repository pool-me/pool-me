//
//  History + Delegate.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/23/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



//MARK: - Delegate -

extension HistoryController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = HistoryHeaderView()
        view.cleanButton.addTarget(self, action: #selector(cleanButtonDidTapped), for: .touchUpInside)
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.backgroundColor = .tertiarySystemGroupedBackground
    }
    
    @objc private func cleanButtonDidTapped() {
//        guard self.history?.count != 0 else { return }
//        self.delegate?.removeAllHistoryOrdering(self)
        
        print("clean buttin is working")
    }
}

