//
//  HistoryController.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class HistoryController: UITableViewController {
    //MARK: - Properties -
    
    var history: [History]?
    
    //MARK: - Dependencies -
    
    weak var coordinator: HistoryCoordinator?
    
    
    
    //MARK: - Init -
    
    init?(coder: NSCoder, coordinator: HistoryCoordinator) {
        self.coordinator = coordinator
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    //MARK: - Lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createTestedHistory()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigationBar()
    }
    
    
    
    //MARK: - Methods -
    
    private func createTestedHistory() {
        self.history = History.testedHistory()
        self.tableView.reloadData()
    }
    
    private func configureNavigationBar() {
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.prefersLargeTitles = true
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithTransparentBackground()
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white,
                                                     .font: UIFont.timesNewRomanLarge]
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white,
                                                .font: UIFont.timesNewRomanStandart]
        navBarAppearance.backgroundColor = .systemPurple
        self.navigationController?.navigationBar.standardAppearance = navBarAppearance
        self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        
        self.navigationItem.title = "History"
    }
}
