//
//  HistoryHeaderView.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/23/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class HistoryHeaderView: BaseView {
    //MARK: - Properties -
    
    let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Delete History"
        label.font = UIFont.timesNewRomanWith(size: 25)
        label.textColor = .black
        return label
    }()
    
    let cleanButton: UIButton = {
        let cleanButton = UIButton(type: .system)
        cleanButton.setImage(#imageLiteral(resourceName: "garbage2"), for: .normal)
        cleanButton.translatesAutoresizingMaskIntoConstraints = false
        cleanButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        return cleanButton
    }()
    
    //MARK: - Methods -
    
    override func setView() {
        super.setView()
        self.drawStackView()
    }
    
    private func drawStackView() {
        let stackView = UIStackView(arrangedSubviews: [label, cleanButton])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = .init(top: 0, left: 20, bottom: 0, right: 10)
        self.addSubview(stackView)
        stackView.fillSuperview()
    }
}

