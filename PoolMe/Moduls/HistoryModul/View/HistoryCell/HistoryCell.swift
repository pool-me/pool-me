//
//  HistoryCell.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/23/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class HistoryCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var historyDateLabel: UILabel!
    @IBOutlet weak var historyFromLabel: UILabel!
    @IBOutlet weak var historyToLabel: UILabel!
    @IBOutlet weak var historyMoneyLabel: UILabel!
    
    //MARK: - Methods -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureBackView()
    }
    
    private func configureBackView() {
        self.mainView.layer.cornerRadius = 12
        self.mainView.setShadow(color: UIColor.lightGray.cgColor,
                                offset: .init(width: 0, height: 1),
                                radius: 3, opacity: 0.4)
    }
    
    func configureCell(model: History?) {
        guard let model = model else { return }
        self.historyDateLabel.text    = model.date
        self.historyFromLabel.text    = "from: \(model.from ?? "")"
        self.historyToLabel.text      = "to: \(model.to ?? "")"
        self.historyMoneyLabel.text   = model.money
    }
}
