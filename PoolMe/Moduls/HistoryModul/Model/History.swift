//
//  History.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/23/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



struct History: Codable {
    let date: String?
    let from: String?
    let to: String?
    let money: String?
    
    static func testedHistory() -> [History] {
        var histories = [History]()
        let first = History(date: "November 3, 2020", from: "Erevan", to: "Gyumri", money: "800 ֏")
        let second = History(date: "January 12, 2020", from: "Erevan", to: "Vanadzor", money: "650 ֏")
        let third = History(date: "April 22, 2020", from: "Gyumri", to: "Erevan", money: "700 ֏")
        histories.append(first)
        histories.append(second)
        histories.append(third)
        return histories
    }
}
