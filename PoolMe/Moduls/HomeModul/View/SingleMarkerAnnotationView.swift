//
//  SingleMarkerAnnotationView.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 3/7/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit
import MapKit



class SingleMarkerAnnotationView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let annotation = newValue as? BaseAnnotation else { return }
            canShowCallout = true
            calloutOffset = CGPoint(x: 0, y: 6)
            //rightCalloutAccessoryView = DirectionAccessoryButton(type: .custom)
            glyphImage = annotation.glyphImage
            markerTintColor = annotation.markerColor
//            let label = DetailCalloutAccessoryLabel()
//            label.text = annotation.subtitle
//            detailCalloutAccessoryView = label
//            leftCalloutAccessoryView = InfoAccessoryButton(type: .custom)
        }
    }
}


