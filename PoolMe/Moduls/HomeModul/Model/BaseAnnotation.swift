//
//  BaseAnnotation.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 3/7/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit
import MapKit



protocol AnnotationStyle: class {
    var markerColor: UIColor? { get }
    var image: UIImage? { get }
    var glyphImage: UIImage? { get }
}

typealias Markable = Descriptionable & Locationable

class BaseAnnotation: NSObject, MKAnnotation, AnnotationStyle {
    let model: Markable
    var title: String? { return model.description.name }
    var subtitle: String? {  return model.description.info }
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: model.location.latitude,
                                      longitude: model.location.longitude)
    }

    var image: UIImage? { return #imageLiteral(resourceName: "heart")}
    var glyphImage: UIImage? { return #imageLiteral(resourceName: "map") }
    var markerColor: UIColor? { return .black }

    required init(model: Markable) { self.model = model }
}




#warning("Locationbale")
protocol Locationable {
    var location: Location { get }
}

struct Location: Codable {
    let latitude: Double
    let longitude: Double
    let address: String?
    let city: String?
}


protocol Descriptionable {
    var description: Description { get }
}


struct Description: Codable {
    let info: String?
    let name: String?
    let imagesUrl: [String]?
}
