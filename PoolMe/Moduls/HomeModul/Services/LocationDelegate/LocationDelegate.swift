//
//  LocationDelegate.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 3/7/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit
import MapKit



class LocationDelegate: NSObject, CLLocationManagerDelegate {
    weak var vc: UIViewController?
    var model: Locationable?
    var locationManager: CLLocationManager
    var mapView: MKMapView

    init(vc: UIViewController?, model: Locationable?, locationManager: CLLocationManager, mapView: MKMapView) {
        self.vc = vc
        self.model = model
        self.locationManager = locationManager
        self.mapView = mapView
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let model = model else {
            let cllocationCoordinate2D = CLLocationCoordinate2D(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!)
            
            let region = MKCoordinateRegion(center: cllocationCoordinate2D, latitudinalMeters: 5000, longitudinalMeters: 5000)
            self.mapView.setRegion(region, animated: true)
            return
        }
        let cllocationCoordinate2D = CLLocationCoordinate2D(latitude: model.location.latitude, longitude: model.location.longitude)
        let region = MKCoordinateRegion(center: cllocationCoordinate2D, latitudinalMeters: 5000, longitudinalMeters: 5000)
        self.mapView.setRegion(region, animated: true)
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.checkLocationAuthorization()
    }

    func showAlertLocation(title: String, message: String?, url: URL?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alert) in
            if let url = url {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        vc?.present(alert, animated: true, completion: nil)
    }

    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            self.locationManager.requestWhenInUseAuthorization()
        case .restricted: print("Parents controll restricted location getting")
        case .denied:
            self.showAlertLocation(title: "You denied use your location", message: "Do you want to allow ?", url: URL(string: UIApplication.openSettingsURLString))
        case .authorizedAlways: break
        case .authorizedWhenInUse:
            self.mapView.showsUserLocation = true
            self.locationManager.startUpdatingLocation()
        @unknown default: print("checkLocationAuthorization unknown default")
        }
    }
}
