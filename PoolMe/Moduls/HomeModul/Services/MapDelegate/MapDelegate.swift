//
//  MapDelegate.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 3/7/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit
import MapKit



class MapDelegate: NSObject, MKMapViewDelegate {
    weak var vc: UIViewController?
    var locationManager: CLLocationManager
    let identifier: String
    //weak var coordinator: PopableMap?

    init(vc: UIViewController, locationManager: CLLocationManager, identifier: String) {
        self.vc = vc
        self.locationManager = locationManager
        //self.coordinator = coordinator
        self.identifier = identifier
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: MKUserLocation.self) { return nil }
        var annotationView: MKAnnotationView?
        annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier, for: annotation)
        return annotationView
    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
//        if control.isMember(of: DirectionAccessoryButton.self) {
//            guard let coordinate = self.locationManager.location?.coordinate else { return }
//            mapView.removeOverlays(mapView.overlays)
//            let user = view.annotation
//            let startpoint = MKPlacemark(coordinate: coordinate)
//            let endpoint = MKPlacemark(coordinate: user!.coordinate)
//
//            let request = MKDirections.Request()
//            request.source = MKMapItem(placemark: startpoint)
//            request.destination = MKMapItem(placemark: endpoint)
//            request.transportType = .automobile
//
//            let direction = MKDirections(request: request)
//            direction.calculate { (response, error) in
//                guard let response = response else { return }
//                DispatchQueue.main.async {
//                    for route in response.routes {
//                        mapView.addOverlay(route.polyline)
//                    }
//                }
//            }
//        } else if control.isMember(of: InfoAccessoryButton.self) {
//            self.coordinator?.popMapScene(from: vc!)
//        }
    }
//
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        let renderer = MKPolylineRenderer(overlay: overlay)
//        renderer.strokeColor = .blue
//        renderer.lineWidth = 4
//        return renderer
//    }
}



