//
//  MapController.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 3/5/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit
import MapKit



class MapController: ABMapController {
    //MARK: - Properties -

    var identifier: String { return NSStringFromClass(BaseAnnotation.self) }

    var annotation: BaseAnnotation? {
        didSet {
            self.mapView.removeAnnotations(self.mapView.annotations)
            self.mapView.addAnnotation(annotation!)
        }
    }
    
    lazy var menuButton: CancelButton = {
        let cancelButton = CancelButton()
        cancelButton.setBackgroundImage(nil, for: .normal)
        cancelButton.setImage(SFImages.menu, for: .normal)
        cancelButton.addTarget(self, action: #selector(menuDidTapped), for: .touchUpInside)
        return cancelButton
    }()

    
    
    //MARK: - Dependencies -

    weak var coordinator: HomeCoordinator?
    var mapDelegate: MapDelegate!
    var locationDelegate: LocationDelegate!



    //MARK: - Init -

    init(coordinator: HomeCoordinator) {
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }



    //MARK: - Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        self.drawMenuButton()
        self.setupLocationDelegate()
        self.setupMapDelegate()
        self.registerMapAnnotationViews()
        self.configureNavigationBar()
        self.showTabBar()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureNavigationBar()
        self.postNotification(name: .orderButtonWillShow)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.postNotification(name: .orderButtonWillShow)
    }
    
    

    //MARK: - Methods -
    
    private func drawMenuButton() {
        self.view.addSubview(self.menuButton)
        self.menuButton.anchor(top: self.view.topAnchor,
                               leading: self.view.leadingAnchor,
                               padding: .init(top: 80, left: 16, bottom: 0, right: 0),
                               size: .init(width: 50, height: 50))
    }

    @objc private func menuDidTapped() {
        self.postNotification(name: .sideBarButtonTapped)
    }
   

    override func registerMapAnnotationViews() {
        self.mapView.register(SingleMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: self.identifier)
    }

    private func setupLocationDelegate() {
        self.locationDelegate = LocationDelegate(vc: self, model: annotation?.model, locationManager: locationManager, mapView: mapView)
        self.locationManager.delegate = locationDelegate
    }

    private func setupMapDelegate() {
        //        self.mapDelegate = MapDelegate(vc: self, locationManager: locationManager, coordinator: coordinator, identifier: identifier)
        self.mapDelegate = MapDelegate(vc: self, locationManager: locationManager, identifier: identifier)
        self.mapView.delegate = mapDelegate
    }
    
    private func configureNavigationBar() {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = .white
        
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithTransparentBackground()
        self.navigationController?.navigationBar.standardAppearance = navBarAppearance
        self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
    }
}





