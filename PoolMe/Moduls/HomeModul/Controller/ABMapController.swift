//
//  ABMapController.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 3/7/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit
import MapKit



class ABMapController: UIViewController {
    
    //MARK: - Properties -
    
    let locationManager = CLLocationManager()
    let mapView = MKMapView()
    
    
    
    //MARK: - Life Cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupMapView()
        self.registerMapAnnotationViews()
        self.drawMapView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkLocationEnabled()
        self.mapView.removeOverlays(self.mapView.overlays)
    }
    
    
    
    
    //MARK: - Methods -
    
    func drawMapView() {
        self.view.addSubview(self.mapView)
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.mapView.fillSuperview()
    }
    
    func setupMapView() {
        self.mapView.showsTraffic = true
        self.mapView.showsScale = true
        self.mapView.showsCompass = true
        self.mapView.userTrackingMode = .follow
    }
    
    func setupManager() {
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func registerMapAnnotationViews() {
        
    }
    
    func showAlertLocation(title: String, message: String?, url: URL?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alert) in
            if let url = url {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            self.locationManager.requestWhenInUseAuthorization()
        case .restricted: print("Parents controll restricted location getting")
        case .denied:
            self.showAlertLocation(title: "You denied use your location", message: "Do you want to allow ?", url: URL(string: UIApplication.openSettingsURLString))
        case .authorizedAlways: break
        case .authorizedWhenInUse:
            self.mapView.showsUserLocation = true
            self.locationManager.startUpdatingLocation()
        @unknown default: print("checkLocationAuthorization unknown default")
        }
    }
    
    func checkLocationEnabled() {
        if CLLocationManager.locationServicesEnabled() {
            self.setupManager()
            self.checkLocationAuthorization()
        } else {
            self.showAlertLocation(title: "Geolocation services turn off in your iphone", message: "Do you want to turn on ?", url: URL(string: "App-Prefs:root=LOCATION_SERVICES"))
        }
    }
}

