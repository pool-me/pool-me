//
//  Signup + TextFieldConfig.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



//MARK: - TextFieldsConfigurations -

extension SignupController {
    func configureTextFields() {
        self.textFieldsPadding()
        for textField in self.textFiedls {
            textField.leftImageTintColor = .forgetPassword
            textField.delegate           = self
        }
        self.passwordTextField.leftImage    = #imageLiteral(resourceName: "key")
        self.emailTextField.leftImage       = SFImages.mail
        self.nameTextField.leftImage        = SFImages.person
        self.phoneNumberTextField.leftImage = SFImages.phone
    }
    
    fileprivate func textFieldsPadding() {
        switch UIDevice().type {
        case .iPhone6, .iPhone6S, .iPhone7, .iPhoneSE, .iPhone8: self.textFieldsStackView.spacing = 8
        case .iPhone6Plus, .iPhone6SPlus, .iPhone7Plus, .iPhone8Plus: self.textFieldsStackView.spacing = 10
        case .iPhoneX, .iPhoneXS, .iPhoneXR: self.textFieldsStackView.spacing = 12
        case .iPhoneXSMax: self.textFieldsStackView.spacing = 16
        case .iPhone11Pro, .iPhone11: self.textFieldsStackView.spacing = 16
        case .iPhone11ProMax: self.textFieldsStackView.spacing = 16
        default: self.textFieldsStackView.spacing = 14
        }
    }
    
    func textFieldsValidation() -> Bool {
        guard let text = self.emailTextField.text, text.isValidEmail else {
            self.emailTextField.shakeTextField()
            //MBProgressHUD.progressHud(to: self.view, text: AuthTextFieldError.incorrectEmail.rawValue, image: #imageLiteral(resourceName: "sorry"), completion: nil)
            return false
        }
        
        guard let password = self.passwordTextField.text, password.count >= 6 else {
            self.passwordTextField.shakeTextField()
            //self.confirmPassword.shakeTextField()
            //MBProgressHUD.progressHud(to: self.view, text: AuthTextFieldError.invalidPassword.rawValue, image: #imageLiteral(resourceName: "sorry"), completion: nil)
            return false
        }
        
        guard self.nameTextField.text != "" else {
            self.nameTextField.shakeTextField()
            //MBProgressHUD.progressHud(to: self.view, text: AuthTextFieldError.nameIsEmtpy.rawValue, image: #imageLiteral(resourceName: "sorry"), completion: nil)
            return false
        }
        
        guard self.numberIsValid, let number = self.phoneNumberTextField.text, number.count == 9 else {
            self.phoneNumberTextField.shakeTextField()
            //MBProgressHUD.progressHud(to: self.view, text: AuthTextFieldError.numberIsInvali.rawValue, image: #imageLiteral(resourceName: "sorry"), completion: nil)
            return false
        }
        
        return true
    }
    
    func resetTextFields() {
        self.textFiedls.forEach { $0.text = "" }
    }
}
