//
//  Signup + TextFieldDelegate.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



extension SignupController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        if text.count == 9 && textField == self.phoneNumberTextField { textField.text!.removeLast() }
        if range.location == 3 && textField == self.phoneNumberTextField {
            let array = ["093", "094", "098", "044", "091", "055", "099", "077"]
            array.contains(text) ? (self.numberIsValid = true) : (self.numberIsValid = false)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.emailTextField   : self.passwordTextField.becomeFirstResponder()
        case self.passwordTextField: self.nameTextField.becomeFirstResponder()
        case self.nameTextField    : self.phoneNumberTextField.becomeFirstResponder()
        default: return false
        }
        return true
    }
}

