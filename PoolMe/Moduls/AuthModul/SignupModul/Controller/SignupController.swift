//
//  SignupController.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class SignupController: UIViewController {
    //MARK: - Outlets -

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var emailTextField: ABTextField!
    @IBOutlet weak var passwordTextField: ABTextField!
    @IBOutlet weak var nameTextField: ABTextField!
    @IBOutlet weak var phoneNumberTextField: ABTextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var textFieldsStackView: UIStackView!
    
    
    
    //MARK: - Properties -
    
    lazy var imagePicker: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = ["public.image", "public.movie"]
        return imagePicker
    }()
    
//    let waitingProgress: MBProgressHUD = {
//        let waitingProgress = MBProgressHUD()
//        waitingProgress.mode = .indeterminate
//        waitingProgress.backgroundView.backgroundColor =
//            UIColor(white: 0.2, alpha: 0.3)
//        waitingProgress.contentColor = .white
//        waitingProgress.bezelView.backgroundColor = #colorLiteral(red: 0.2514024675, green: 0.107686244, blue: 0.427895546, alpha: 1)
//        waitingProgress.removeFromSuperViewOnHide = true
//        waitingProgress.offset = CGPoint(x: 0, y:
//            MBProgressMaxOffset)
//        return waitingProgress
//    }()
    
    lazy var textFiedls: [ABTextField] = [self.emailTextField,
                                          self.passwordTextField,
                                          self.nameTextField,
                                          self.phoneNumberTextField]
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent }
    var numberIsValid: Bool = false
    var profileImage: UIImage?
    
    
    
    //MARK: - Dependencies -
    
    weak var coordinator: AuthCoordinator!
//    let authService: AuthService!
//    let keyboardManager: KeyboardService!
    
    
    
    //MARK: - Init -
    
    init(coordinator: AuthCoordinator) {
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK: - Life Cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureSignInButton()
//        self.keyboardManager.setKeyboardNotification()
//        self.keyboardManager.fromView = self.view
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.configureImageView()
        self.configureTextFields()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.keyboardManager.removeKeyboardNotification()
//        self.resetTextFields()
    }
    
    
    
    //MARK: - Methods -
    
    private func configureImageView() {
        self.backgroundImageView.roundCorners(corners:
            .bottomLeft, radius: 80)
        self.imageBackView.setShadow(radius: 4, opacity: 0.45)
        self.signInButton.standartShadow()
    }
    
    private func configureSignInButton() {
        self.signInButton.layer.cornerRadius = 22
        self.signInButton.layer.masksToBounds = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with
        event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    private func signIn() {
//        authService.signIn(user: self.createUser(),
//                           profileImage: self.profileImage) {
//                            [weak self] result in
//                            guard let self = self else { return }
//                            self.view.finishHud()
//                            switch result {
//                            case .success(let user):
//                                MBProgressHUD.progressHud(to:
//                                    self.view, text:
//                                    "Congratulation
//                                    \(user.username).", image: #imageLiteral(resourceName: "wellDone"))
//                                    { [weak self](hud) in
//                                    guard let self = self else {
//                                    return }
//
//                                    self.coordinator
//                                    .driveContainerScene(from:
//                                    self)
//                                }
//                            case .failure(let error):
//                                MBProgressHUD.progressHud(to:
//                                    self.view, text:
//                                    error.localizedDescription,
//                                               image: #imageLiteral(resourceName: "sorry"), completion: nil)
//                            }
//        }
    }
    
    
    private func createDriver()  {
       
    }
    
    private func createPassenger()  {
        
    }
    

    
    
    
    //MARK: - Actions -
    
    @IBAction func addPhotoActions(_ sender: UIButton) {
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func signInButtonAction(_ sender: UIButton) {
//        guard self.textFieldsValidation() else { return }
//        self.view.endEditing(true)
//        self.view.startHud()
//        self.signIn()
    }
    
    @IBAction func logInButtonAction(_ sender: UIButton) {
        self.coordinator?.driveLogInScene(from: self)
    }
}













