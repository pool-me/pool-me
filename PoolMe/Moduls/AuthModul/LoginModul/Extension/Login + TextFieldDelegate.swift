//
//  Login + TextFieldDelegate.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



extension LoginController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.emailTextField   : self.passwordTextField.becomeFirstResponder()
        case self.passwordTextField: self.passwordTextField.resignFirstResponder()
        default: return false
        }
        return true
    }
}
