//
//  LoginController.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class LoginController: UIViewController {
    //MARK: - Outlets -
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var emailTextField: ABTextField!
    @IBOutlet weak var passwordTextField: ABTextField!
    @IBOutlet weak var logInButton: UIButton!
    
    
    
    //MARK: - Properties -
    
    lazy var textFiedls: [ABTextField] = [self.emailTextField,
                                          self.passwordTextField]
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    
    
    //MARK: - Dependencies -
    
    weak var coordinator: AuthCoordinator?
//    var authService: AuthService?
//    var keyboardManager: KeyboardService?
    
    
    
    //MARK: - Init -
    
    init(coordinator: AuthCoordinator) {
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK: - Life Cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureSignInButton()
        self.configureTextFields()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.keyboardManager?.setKeyboardNotification()
//        self.keyboardManager?.fromView = self.view
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkUserPresense()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.configureImageView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.keyboardManager?.removeKeyboardNotification()
    }
    
    
    
    //MARK: - Methods -
    
    private func configureImageView() {
        self.backgroundImageView.roundCorners(corners: .bottomLeft, radius: 80)
        self.imageBackView.setShadow(radius: 4, opacity: 0.45)
        self.logInButton.standartShadow()
    }
    
    private func configureSignInButton() {
        self.logInButton.layer.cornerRadius  = 22
        //self.logInButton.layer.masksToBounds = true
        //        self.logInButton.setGradientColor(#colorLiteral(red: 0.2509803922, green: 0.1058823529, blue: 0.4274509804, alpha: 1), .systemPurple, frame: self.logInButton.bounds)
        
        //        let first  = UIColor(named: "logInButtonFirst")!
        //        let second = UIColor(named: "logInButtonSecond")!
        //        self.logInButton.setGradientColor(first, second, frame: self.logInButton.bounds)
    }
    
    private func configureTextFields() {
        for textField in self.textFiedls {
            textField.leftImageTintColor = .forgetPassword
            textField.delegate           = self
        }
        self.passwordTextField.leftImage    = #imageLiteral(resourceName: "key")
        self.emailTextField.leftImage       = SFImages.mail
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    private func textFieldsValidation() -> Bool {
//        guard let text = self.emailTextField.text, text.isValidEmail else {
//            self.emailTextField.shakeTextField()
//            MBProgressHUD.progressHud(to: self.view, text: AuthTextFieldError.incorrectEmail.rawValue, image: #imageLiteral(resourceName: "sorry"), completion: nil)
//            return false
//        }
//
//        guard self.passwordTextField.text != "" else {
//            self.passwordTextField.shakeTextField()
//            MBProgressHUD.progressHud(to: self.view, text: AuthTextFieldError.invalidPassword.rawValue, image: #imageLiteral(resourceName: "sorry"), completion: nil)
//            return false
//        }
//
        return true
    }
    
    private func logIn() {
//        self.authService?.logIn(email: self.emailTextField.text!, password: self.passwordTextField.text!) { [weak self] (result) in
//            guard let self = self else { return }
//            self.view.finishHud()
//            switch result {
//            case .success(let user):
//                MBProgressHUD.progressHud(to: self.view, text: "Congratulation \(user.username).", image: #imageLiteral(resourceName: "wellDone")) { [weak self](hud) in
//                    guard let self = self else { return }
//                    self.coordinator?.driveContainerScene(from: self)
//                }
//            case .failure(let error):
//                MBProgressHUD.progressHud(to: self.view, text: error.localizedDescription, image: #imageLiteral(resourceName: "sorry"), completion: nil)
//            }
//        }
    }
    
    private func checkUserPresense() {
//        guard let authService = authService else { return }
//        self.view.startHud(y: 0, backgroundViewColor: .darkBackground)
//        DispatchQueue.main.async {
//            authService.currentUser { [weak self] (result) in
//                guard let self = self else { return }
//                switch result {
//                case .success:
//                    self.coordinator?.driveContainerScene(from: self)
//                    self.view.finishHud()
//                case .failure(let response):
//                    self.view.finishHud()
//                    print(response.localizedDescription)
//                }
//            }
//        }
    }
    
    
    
    //MARK: - Actions -
    
    
    @IBAction func forgotPasswordButtonAction(_ sender: UIButton) {
        self.coordinator?.driveForgotPasswordScene(from: self)
    }
    @IBAction func logInButtonAction(_ sender: UIButton) {
        guard self.textFieldsValidation() else { return }
//        self.view.endEditing(true)
//        self.view.startHud()
//        self.logIn()
        self.coordinator?.driveContainerScene(from: self)
    }
    @IBAction func registerButtonAction(_ sender: UIButton) {
        self.coordinator?.driveSignupScene(from: self)
    }
    
}

