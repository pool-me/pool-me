//
//  ForgetPasswordController.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class ForgetPasswordController: UIViewController {
    //MARK: - Outlets -
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var emailTextField: ABTextField!
    @IBOutlet weak var ressetPasswordButton: UIButton!
    
    
    
    //MARK: - Properties -
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    
    
    //MARK: - Dependencies -
    var coordinator: AuthCoordinator!
    //    let authService: AuthService!
    //    let keyboardManager: KeyboardService!
    
    
    
    //MARK: - Init -
    
    init(coordinator: AuthCoordinator) {
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK: - Life Cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureRessetButton()
        //        self.keyboardManager.setKeyboardNotification()
        //        self.keyboardManager.fromView = self.view
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.configureImageView()
        self.configureTextFields()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // self.keyboardManager.removeKeyboardNotification()
    }
    
    
    
    //MARK: - Methods -
    
    private func configureImageView() {
        self.backgroundImageView.roundCorners(corners:
            .bottomLeft, radius: 80)
        self.imageBackView.setShadow(radius: 4, opacity: 0.45)
        self.ressetPasswordButton.standartShadow()
    }
    
    private func configureRessetButton() {
        self.ressetPasswordButton.layer.cornerRadius  = 20
        self.ressetPasswordButton.layer.masksToBounds = true
    }
    
    fileprivate func configureTextFields() {
        self.emailTextField.leftImage = SFImages.mail
        self.emailTextField.leftImageTintColor = .forgetPassword
    }
    
    fileprivate func textFieldsValidation() -> Bool {
        guard let text = self.emailTextField.text,
            text.isValidEmail else {
                self.emailTextField.shakeTextField()
                return false
        }
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with
        event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    //MARK: - Actions -
    
    @IBAction func ressetPasswordButtonAction(_ sender: UIButton) {
        guard self.textFieldsValidation() else { return }
//        MBProgressHUD.configureProgressHud(to: self.view, text: "Loading...", mode: .indeterminate, backgroundColor: .black)
//        self.authService.forgotPassword(email:
//        self.emailTextField.text!) { [weak self] (result)
//            guard let self = self else { return }
//            MBProgressHUD.configureProgressHud(to: self.view,
//                                               text: result.localizedDescription, image: #imageLiteral(resourceName: "complete"))
//            Timer.scheduledTimer(withTimeInterval: 1.3, repeat
//            false) { (timer) in
//                self.coordinator.driveLogInScene(from: self)
//            }
//        }
    }
    
    @IBAction func dismissForgotScene(_ sender: UIButton) {
        self.coordinator.driveLogInScene(from: self)
    }
}
