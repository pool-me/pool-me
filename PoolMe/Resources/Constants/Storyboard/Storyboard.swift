//
//  Storyboard.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/6/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import Foundation


struct Storyboard {
    static let main = "Main"
    static let menu = "SideMenuController"
    static let history = "HistoryController"
}


