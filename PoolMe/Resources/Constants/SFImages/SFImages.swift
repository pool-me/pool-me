//
//  SFImages.swift
//  PoolMe
//
//  Created by Aleksandr Bagdasaryan on 3/22/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


struct SFImages {
    static let mail   = UIImage(systemName: "envelope.fill")!
    static let person = UIImage(systemName: "person.fill")!
    static let phone  = UIImage(systemName: "phone.fill")!
    static let menu   = UIImage(systemName: "text.alignleft")!
}
