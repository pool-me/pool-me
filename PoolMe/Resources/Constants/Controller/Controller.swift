//
//  Controller.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 12/6/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import Foundation


struct Controller {
    static let menu = "MenuBarController"
    static let history = "HistoryController"
}
