//
//  CancelButton.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 3/17/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class CancelButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setButton()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setButton()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.standartShadow()
        self.layer.cornerRadius = 20
    }
    
    func setButton() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = .systemGray6 //UIColor(white: 1, alpha: 0.5)
        self.tintColor =  .black //.systemGray2 //UIColor(named: "abText")  //.white
        let image = UIImage(systemName: "xmark.circle.fill")
        self.setBackgroundImage(image, for: .normal)
    }
}
