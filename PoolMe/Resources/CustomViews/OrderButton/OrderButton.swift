//
//  OrderButton.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 2/4/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class OrderButton: UIButton {
    
    //MARK: - Properties -
    
    var fontSize: CGFloat = 22 {
        didSet {
            print(fontSize)
            let size = fontSize < 22 ? 22 : fontSize
            let size2 = fontSize > 27 ? 27 : size
            let attributedString = NSAttributedString(string: "Pool Me!",
                                                      attributes: [.foregroundColor : UIColor.white,
                                                                   .font : UIFont.init(name: "Times New Roman", size: size2)!])
            setAttributedTitle(attributedString, for: .normal)
        }
    }
    
    
    //MARK: - Init -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureOrderButton()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.configureOrderButton()
        fatalError("init(coder:) has not been implemented")
    }
    

    
    //MARK: - Methods -
    
    private func configureOrderButton() {
        layer.cornerRadius = 19
        clipsToBounds = true
        layer.masksToBounds = true
        
        let attributedString = NSAttributedString(string: "Pool Me!",
                                                  attributes: [.foregroundColor : UIColor.white,
                                                               .font : UIFont.init(name: "Times New Roman", size: 22)!])
        setAttributedTitle(attributedString, for: .normal)
        backgroundColor = .systemPurple
        translatesAutoresizingMaskIntoConstraints = false
        self.standartShadow()
        //self.setShadow(color: #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1), offset: .init(width: 0, height: 1), radius: 2, opacity: 1)
    }
}

