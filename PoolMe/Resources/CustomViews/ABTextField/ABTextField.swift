//
//  ABTextField.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 11/20/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


class ABTextField: UITextField {
    
    
    //MARK: - Init -
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setTextField()
    }
    
    
    
    //MARK: - Properties -
   
    override var placeholder: String? {
        didSet {
            guard let placeholder = placeholder else { return }
            attributedPlaceholder = NSAttributedString(string: placeholder,
                                                       attributes: [.foregroundColor : placeholderColor,
                                                                    .font: UIFont(name: "Times New Roman", size: 19)!])
            
        }
    }
    
    var placeholderColor: UIColor = .placeholderText {
        didSet {
            attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                       attributes: [.foregroundColor : placeholderColor,
                                                                    .font: UIFont(name: "Times New Roman", size: 19)!])
        }
    }
    
    private let leftImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    
    
    //MARK: - Inspectables -
    
    var leftImage: UIImage? {
        didSet {
            self.leftImageView.image = self.leftImage
        }
    }
    
    var leftImageTintColor: UIColor? {
        didSet {
            self.leftImageView.tintColor = self.leftImageTintColor
        }
    }
    
    var cornerRadius: CGFloat? {
        didSet {
            guard let cornerRadius = cornerRadius else { return }
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    
    
    //MARK: - Methods -
    
    private func setLeftImageView() {
        self.addSubview(self.leftImageView)
        self.leftImageView.anchor(top: self.topAnchor,
                                  leading: self.leadingAnchor,
                                  bottom: self.bottomAnchor,
                                  trailing: nil,
                                  padding: .init(top: 5, left: 15, bottom: 5, right: 0),
                                  size: .init(width: 30, height: 0))
    }
    
    private func setTextField() {
        self.setLeftImageView()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.textAlignment = .center
        self.autocorrectionType = .no
        self.keyboardAppearance = .dark
        self.backgroundColor = .systemGray6 //.white //UIColor.rgb(242, 242, 247)
        self.borderStyle = .none
        self.layer.cornerRadius = 16
        self.textColor = .label
        self.font = UIFont(name: "Times New Roman", size: 19)!
        self.standartShadow()
    }
    
    enum ImageDirection {
        case left
        case right
    }
    
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 40, y: bounds.origin.y, width: bounds.width - 80, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
         return CGRect(x: bounds.origin.x + 50, y: bounds.origin.y, width: bounds.width - 80, height: bounds.height)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    
    
    func setImageForTextField(imageDirection: ImageDirection, image: UIImage?, tintColor: UIColor?) {
        let imageView = UIImageView(image: image)
        imageView.tintColor = tintColor
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 5
       
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 18))
        view.addSubview(imageView)
        imageView.setSize(size: .init(width: 25, height: 20))
        
        switch imageDirection {
        case .left: imageView.fillSuperview(padding: UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))
        leftView = view
        case .right: imageView.fillSuperview(padding: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10))
        rightView = view
        }
    }
    
    func shakeTextField() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 4, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 4, y: self.center.y))
        self.layer.add(animation, forKey: "position")
    }
}


