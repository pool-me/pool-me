//
//  RoundShadowView.swift
//  Ivi
//
//  Created by Aleksandr Bagdasaryan on 11/3/19.
//  Copyright © 2019 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit


class RoundShadowView: UIView {
  
    let containerView = UIView()
    var cornerRadius: CGFloat = 35.0
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layoutView()
    }
 
    
    override func layoutSubviews() {
        super.layoutSubviews()

        containerView.roundCorners(corners: [.topRight, .topLeft], radius: cornerRadius)
        containerView.clipsToBounds = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layoutView()
    }

    private func layoutView() {
        layer.cornerRadius = cornerRadius
        self.setShadow(offset: .init(width: 0, height: -4), radius: 2.4, opacity: 0.2)
        translatesAutoresizingMaskIntoConstraints = true
        addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        containerView.backgroundColor = .white //.systemBackground
    }
}
