//
//  ABLabel.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 1/11/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit




@IBDesignable
class ABLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setInitialValues()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setInitialValues()
    }
    
    private func setInitialValues() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.textColor = .white
        self.backgroundColor = UIColor.systemPurple.withAlphaComponent(0.85)
        self.font = UIFont.boldSystemFont(ofSize: 16)
        self.textAlignment = .center
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius  = 9
        self.layer.masksToBounds = true
        self.layer.borderWidth   = 2
        self.layer.borderColor   = UIColor.systemPurple.cgColor // #colorLiteral(red: 0.297534734, green: 0.5247463584, blue: 0.1552932858, alpha: 1).cgColor
    }
    
    override var intrinsicContentSize: CGSize {
        let height = super.intrinsicContentSize.height
        let width  = super.intrinsicContentSize.width + 16
        return .init(width: width, height: height)
    }
}



