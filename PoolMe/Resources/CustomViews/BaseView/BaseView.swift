//
//  BaseView.swift
//  Evenation
//
//  Created by Aleksandr Bagdasaryan on 1/12/20.
//  Copyright © 2020 Aleksandr Bagdasaryan. All rights reserved.
//

import UIKit



class BaseView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setView()
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setView()
    }
    
    func setView() {}
}

